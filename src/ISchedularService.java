import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.Set;

@Path("/api/schedular")
@Produces("application/json")
public interface ISchedularService {
    /**
     * This function creates a meeting with the given values.
     * If the Id is null or not found in the db then it will create a new meeting.
     * If the meeting is found, it will simply update the existing meeting.
     */
    @POST
    @Path("/createMeeting")
    public Response createMeeting(@DefaultValue("null") @QueryParam("id") Integer id,
                                  @QueryParam("start-date") Date start,
                                  @QueryParam("end-date") Date finish,
                                  @QueryParam("description") String description,
                                  @QueryParam("attendees") final Set<String> attendeesEmails);

    /**
     * This function should check for schedule conflicts in a given meeting.
     * Although the UI can prevent user error, It is still possible to try to schedule a meeting on exposed api.
     * Make sure to ignore the current meeting.
     */
    boolean hasConflict(Meeting meeting);

    /**
     * This function will get possible times for meetings. It should do this through the SchedulerEngine
     * This can find the group memebers in the DB by email add them to a set and pass them on to the SchedulerEngine.
     * @return Set of dates where all group members are available to meet.
     */
    @GET
    @Path("/findGroupAvailability")
    Set<Date> findGroupAvailability(
            @QueryParam("start-date") Date start,
            @QueryParam("end-date") Date finish,
            @QueryParam("attendees") final Set<String> attendeesEmails);
}