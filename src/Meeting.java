@Entity
public class Meeting {
    @Id
    private int id;

    private Date start;

    private Date finish;

    private String description;

    @OneToMany
    private Set<Person> attendees;
}
