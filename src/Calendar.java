@Entity
public class Calendar {
    @Id
    private int id;

    private int month;

    private int year;

    @OneToMany
    private Set<Meeting> meetings;

    @OneToMany
    private Set<Person> sharedWith;
}