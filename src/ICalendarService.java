import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/api/calendar")
@Produces("application/json")
public interface ICalendarService {
//    User needs to
//      - View a monthly calendar where all meetings are shown
//      - See the meeting details which include:
//          = attendees (Included in meeting object)
//          = Meeting date (Also included in meeting object)
//      - User must be able to reschedule meeting
//      - Must be able to create a new meeting and select attendees


    /**
     * This function gets a calendar containing all meetings this month.
     * If none exists, then it creates one.
     * @return Calendar - a Calendar containing all meetings this month.
     */
    @GET
    @Path("/baseCalendar")
    public Calendar getBaseCalendar();

///////////////////////////////////////////// Extras?

    /**
     * This function should return a user's calendars such that the UI can display all of their meetings.
     * If there is no calendar associated with the user ID within the db, create a new calendar for them.
     * Be sure to add this calendar to the DB. It's not worth rebuilding someone's calendar every single time.
     * @return Calendar - a calendar containing all of the meetings that a particular user has.
     */
    @GET
    @Path("/getUserCalendar")
    public Calendar getUserCalendar(); // While this is not a feature asked for in the assignment, it makes sense that a user would want to see their own calendar.

}
