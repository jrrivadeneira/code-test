This is our code test. Please read the entire assignment before proceeding.

Please read through the exercise fully before starting any work. You have 7 full days to complete the exercise but it should not take more than a couple hours.

You have been tasked with designing a JaxRS ReSTful API to expose operations for a calendar application. Provide Java Interfaces that specify the API, **do not implement any business logic**.

The entities exposed by this API are(constructors, accessors and mutators excluded for brevity):
```java
@Entity
public class Person {
   @Id
   private int id;
    
   private String name;

   private String email;
}

@Entity
public class Meeting {
   @Id
   private int id;

   private Date start;

   private Date finish;

   private String description;

   @OneToMany
   private Set<Person> attendees;
}

@Entity
public class Calendar {
   @Id
   private int id;
   
   private int month;

   private int year;

   @OneToMany
   private Set<Meeting> meetings;

   @OneToMany
   private Set<Person> sharedWith;
}
```
#The backend systems include: 

## SchedulerEngine (do not implement) 
- Creates a list of times when a set of people are free to attend a meeting on a given day.
## CalenderSyncJob (do not implement)
- Synchronizes calendar entries with another calendar system. Does not export attendees, only meetings for each calendar month.

# The frontend UIs include:

## MonthlyCalendarApp (do not implement)
- Shows the user a monthly calendar which shows all meetings for the month. 
- A user can click on a meeting to see the attendees or reschedule.
## SchedularApp (do not implement)
- Allows a user to schedule a meeting including selecting attendees for the meeting.

# Your task:
- Please create one or more service interfaces providing an API with enough functionality to meet the needs of these systems and UIs. Limit duplication of functionality as much as possible, but consider scalability in doing so.

- Describe how your API solves each of the use cases for the backend systems and UIs.